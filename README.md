# README #

### What is this repository for? ###

This is a quick sample project to demonstrate hiding the mouse cursor

### How do I get set up? ###

Clone the repository and open the project in Xcode. There are no dependencies so you should be able to build it right away.

### Contribution guidelines ###

Follow the Swift Developer Guidelines

### Who do I talk to? ###

* Keahi Ertell
* Justin Tyre