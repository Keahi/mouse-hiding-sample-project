//
//  RenderWindowsManager.swift
//  NoMouseOverWindowTesting
//
//  Created by Keahi Ertell on 1/23/18.
//  Copyright © 2018 Keahi Ertell. All rights reserved.
//

import Cocoa
import Foundation


class RenderWindowsManager {
	/// An array of `RenderWindow` objects used by the application
	var windows: [RenderWindow] {
		didSet {
			NSLog("Windows array contains:")
			windows.forEach { NSLog("    \($0)") }
		}
	}
	
	
	init() {
		windows = []
	}
	
	
	
	// MARK: - Coordinate Translation
	
	
	
	// MARK: - Window Management
	
	/// Makes a new window at the given coordinates with the given size
	func createWindow(x: Int, y: Int, width: Int, height: Int) {
		let rect = NSRect(x: x, y: y, width: width, height: height)
		let window = RenderWindow(contentRect: rect, styleMask: .borderless, backing: .buffered, defer: false)
		window.orderFront(nil)
		windows.append(window)
	}
	
	
	func hideAllWindows() {
		windows.forEach { $0.miniaturize(nil) }
	}
	
	
	func unhideAllWindows() {
		windows.forEach { $0.deminiaturize(nil) }
	}
	
	
	func closeAllWindows() {
		windows.forEach { $0.orderOut(nil) }
		windows.removeAll()
	}
}
