//
//  AppDelegate.swift
//  NoMouseOverWindowTesting
//
//  Created by Keahi Ertell on 1/23/18.
//  Copyright © 2018 Keahi Ertell. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate, NSWindowDelegate {
	
	var windowsManager: RenderWindowsManager

	@IBOutlet weak var window: NSWindow!
	@IBOutlet weak var createWindowButton: NSButton!
	
	@IBOutlet weak var xField: NSTextField!
	@IBOutlet weak var yField: NSTextField!
	
	@IBOutlet weak var widthField: NSTextField!
	@IBOutlet weak var heightField: NSTextField!
	
	
	/// Called to create a new `RenderWindow` object
	@IBAction func createWindow(_ sender: Any) {
		windowsManager.createWindow(x: xField.integerValue, y: yField.integerValue, width: widthField.integerValue, height: heightField.integerValue)
	}
	
	
	@IBAction func deleteAllWindows(_ sender: Any) {
		windowsManager.closeAllWindows()
	}
	
	
	
	// MARK: - NSApplicationDelegate
	
	override init() {
		windowsManager = RenderWindowsManager()
		super.init()
	}
	
	
	func applicationDidFinishLaunching(_ aNotification: Notification) {
		// Insert code here to initialize your application
		window.delegate = self
	}

	func applicationWillTerminate(_ aNotification: Notification) {
		// Insert code here to tear down your application
	}

	
	
	// MARK: - NSWindowDelegate
	
	func windowDidMiniaturize(_ notification: Notification) {
		windowsManager.hideAllWindows()
	}
	
	
	func windowDidDeminiaturize(_ notification: Notification) {
		windowsManager.unhideAllWindows()
	}
	
	
	func windowDidResignKey(_ notification: Notification) {
		windowsManager.hideAllWindows()
	}
	
	
	func windowDidBecomeKey(_ notification: Notification) {
		windowsManager.unhideAllWindows()
	}
	
	
	func windowWillClose(_ notification: Notification) {
		// I did this to make this program less annoying to run and debug
		NSApplication.shared.terminate(nil)
	}
}

