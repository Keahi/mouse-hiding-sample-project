//
//  RenderWindow.swift
//  NoMouseOverWindowTesting
//
//  Created by Keahi Ertell on 1/23/18.
//  Copyright © 2018 Keahi Ertell. All rights reserved.
//

import Cocoa
import Foundation



enum WindowQuadrant {
	case down
	case right
	case up
	case left
	
	static let all: [WindowQuadrant] = [.down, .right, .up, .left]
}



class RenderWindow: NSWindow {
	
	// MARK: - Computed View Frame Properties
	
	var viewWidth: CGFloat {
		guard let contentView = contentView else { return 0 }
		return contentView.frame.width
	}
	
	
	var viewHeight: CGFloat {
		guard let contentView = contentView else { return 0 }
		return contentView.frame.height
	}
	
	
	var centerX: CGFloat {
		return viewWidth / 2
	}
	
	
	var centerY: CGFloat {
		return viewHeight / 2
	}
	
	
	
	// MARK: - Window Configuration
	
	override init(contentRect: NSRect, styleMask style: NSWindow.StyleMask, backing backingStoreType: NSWindow.BackingStoreType, defer flag: Bool) {
		super.init(contentRect: contentRect, styleMask: style, backing: backingStoreType, defer: flag)
		configureWindow()
	}
	
	
	/// Called to set up basic window settings
	private func configureWindow() {
		self.backgroundColor = NSColor.systemPink
		self.acceptsMouseMovedEvents = true
		self.level = .screenSaver
		self.contentView = NSView(frame: self.frame)
		
		let rect = NSRect(origin: .zero, size: self.frame.size)
		let view = NSView(frame: rect)
		view.addTrackingArea(NSTrackingArea(rect: rect, options: [.activeAlways, .mouseEnteredAndExited], owner: self, userInfo: [:]))
		
		contentView = view
	}
	
	
	
	// MARK: - Window Events
	
	override func mouseEntered(with event: NSEvent) {
		super.mouseEntered(with: event)
		NSCursor.hide()
		moveCursorThroughWindow(event)
	}
	
	
	/// Immediately moves the cursor from one side of the window to the other
	/// - Parameters
	///   - event: The `mouseEntered` event to handle
	private func moveCursorThroughWindow(_ event: NSEvent) {
		guard let quadrant = quadrantFor(event.locationInWindow) else { return }
		
		// Translate this into screen coordinates
		var location = event.locationInWindow
		location.x += self.frame.origin.x
		location.y += self.frame.origin.y
		
		switch quadrant {
		case .down:
			location.y += viewHeight
			break
		case .right:
			location.x -= viewWidth
			break
		case .up:
			location.y -= viewHeight
			break
		case .left:
			location.x += viewWidth
			break
		}
		
		// Translate again
		// TODO: Make this work for multiple displays
		let screenHeight = event.window?.screen?.visibleFrame.height ?? 0
		let deviceDisplayID = (event.window?.screen?.deviceDescription[NSDeviceDescriptionKey.init("NSScreenNumber")] as? UInt32) ?? CGMainDisplayID()
		location.y = screenHeight - location.y
		CGDisplayMoveCursorToPoint(CGMainDisplayID(), location)
	}
	
	
	override func mouseExited(with event: NSEvent) {
		super.mouseExited(with: event)
		NSCursor.unhide()
	}
	
	
	// MARK: - Window "Quadrant" Coordinates
	
	/// - Parameters:
	///   - coordinate: The location of the mouse pointer in relation to the window
	/// - Returns: The `WindowQuadrant` the mouse pointer entered
	private func quadrantFor(_ coordinate: CGPoint) -> WindowQuadrant? {
		return WindowQuadrant.all.first { return coordinateBelongs(coordinate, in: $0) }
	}
	
	
	/// - Parameters:
	///   - coordinate: The location of the mouse pointer in relation to the window
	///   - quadrant: The quadrant to test
	/// - Returns: True, if the coordinate is in the specified quadrant. False, otherwise
	private func coordinateBelongs(_ coordinate: CGPoint, in quadrant: WindowQuadrant) -> Bool {
		let bottomLeft = NSPoint(x: 0, y: 0)
		let bottomRight = NSPoint(x: viewWidth, y: 0)
		let topRight = NSPoint(x: viewWidth, y: viewHeight)
		let topLeft = NSPoint(x: 0, y: viewHeight)
		let center = NSPoint(x: centerX, y: centerY)
		
		switch quadrant {
		case .down:
			let down = NSBezierPath()
			down.move(to: center)
			down.line(to: bottomLeft)
			down.line(to: bottomRight)
			down.line(to: center)
			return down.contains(coordinate)
		case .right:
			let right = NSBezierPath()
			right.move(to: center)
			right.line(to: topRight)
			right.line(to: bottomRight)
			right.line(to: center)
			return right.contains(coordinate)
		case .up:
			let up = NSBezierPath()
			up.move(to: center)
			up.line(to: topLeft)
			up.line(to: topRight)
			up.line(to: center)
			return up.contains(coordinate)
		case .left:
			let left = NSBezierPath()
			left.move(to: center)
			left.line(to: topLeft)
			left.line(to: bottomLeft)
			left.line(to: center)
			return left.contains(coordinate)
		}
	}
}



extension RenderWindow {
	/// - Returns: An always-positive value for the given `CGFloat`
	func abs(_ value: CGFloat) -> CGFloat {
		if value < 0 {
			return value * -1.0
		} else {
			return value
		}
	}
}
